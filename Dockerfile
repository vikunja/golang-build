FROM golang:1.23-alpine
ENV GO111MODULE=on
RUN apk --no-cache add build-base git && \ 
  go install github.com/magefile/mage@latest && \
  mv /go/bin/mage /usr/local/go/bin # Since we're mounting the source we want to compile into /go, we will overlay the mage binary

