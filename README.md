# golang-build

[![Build Status](https://drone.kolaente.de/api/badges/vikunja/golang-build/status.svg)](https://drone.kolaente.de/vikunja/golang-build)

A docker image to build go applications with module support enabled.
